<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VendaProduto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         //cria as tabelas do banco
         Schema::create("VendaProduto",
         function($tabela){
           $tabela->increments("ID_VENDAPRODUTO");
           $tabela->integer("ID_VENDA")->unsigned();
           $tabela->integer("ID_PRODUTO")->unsigned();
           $tabela->foreign('ID_PRODUTO', 'FK_VENDAPRODUTO_PRODUTO')
              ->references('ID_PRODUTO')->on('Produto');
           $tabela->foreign('ID_VENDA', 'FK_VENDAPRODUTO_VENDA')
              ->references('ID_VENDA')->on('Venda');
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
