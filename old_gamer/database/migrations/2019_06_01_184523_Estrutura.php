<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Estrutura extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //cria as tabelas do banco
        Schema::create("Produto",
          function($tabela){
            $tabela->increments("ID_PRODUTO");
            $tabela->string("DESCRICAO");
            $tabela->decimal("VALOR");
            $tabela->binary("IMAGEM");
            $tabela->enum("TIPO_PRODUTO",array("console","jogo"));
          });

        Schema::create("Cliente",
          function($tabela){
            $tabela->increments("ID_CLIENTE");
            $tabela->string("NOME_COMPLETO");
            $tabela->string("CPF");
            $tabela->string("RG");
            $tabela->string("EMAIL");
            $tabela->string("SENHA");
            $tabela->bigInteger("TELEFONE1");
            $tabela->bigInteger("TELEFONE2");
            $tabela->date("DATA_NASCIMENTO");
        });

        Schema::create("Endereco",
          function($tabela){
            $tabela->increments("ID_ENDERECO");
            $tabela->integer("ID_CLIENTE")->unsigned();
            $tabela->string("ESTADO");
            $tabela->string("CIDADE");
            $tabela->string("BAIRRO");
            $tabela->integer("NUMERO");
            $tabela->string("LOGRADOURO");
            $tabela->string("COMPLEMENTO");
            $tabela->foreign('ID_CLIENTE', 'FK_ENDERECO_CLIENTE')
              ->references('ID_CLIENTE')->on('Cliente');
        });

        Schema::create("Admin",
          function($tabela){
            $tabela->increments("ID_ADMIN");
            $tabela->string("EMAIL");
            $tabela->string("SENHA");
        });

        Schema::create("Venda",
          function($tabela){
            $tabela->increments("ID_VENDA");
            $tabela->integer("ID_CLIENTE")->unsigned();
            $tabela->integer("ID_PRODUTO")->unsigned();
            $tabela->decimal("VALOR_TOTAL");

            $tabela->foreign('ID_PRODUTO', 'FK_PRODUTO')
              ->references('ID_PRODUTO')->on('Produto');

            $tabela->foreign('ID_CLIENTE', 'FK_CLIENTE')
              ->references('ID_CLIENTE')->on('Cliente');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
