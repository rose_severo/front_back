<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'InicioController@index')->name('index');
Route::match(['get', 'post'], '/login', 'LoginController@index')->name('login');
Route::get('/logout', 'LoginController@logout')->name('logout');
Route::group(['prefix' => 'cliente'], function() {
    Route::match(['get', 'post'], '/cadastro', 'ClienteController@cadastro')->name('cliente.cadastro');
    Route::match(['get', 'post'], '/editar', 'ClienteController@editar')->name('cliente.editar');
});
Route::get('/produtos/{tipoProduto?}', 'ProdutoController@index')->name('produto.index');
Route::get('/carrinho', 'CarrinhoController@index')->name('carrinho.index');
Route::get('/carrinho/remover/{indice}', 'CarrinhoController@removeCarrinho')->name('carrinho.remover');
Route::get('/carrinho/finalizar', 'CarrinhoController@finalizar')->name('carrinho.finalizar');
Route::get('/contato', function() {
    return view('contato/index');
})->name('contato');

Route::group(['prefix' => 'api'], function() {
    Route::get('/produto/{id}', 'ProdutoController@apiGetProduct')->name('api.produto.get');
    Route::get('/add_carrinho/{produtoId}', 'CarrinhoController@addCarrinho')->name('api.carrinho.add');
});
