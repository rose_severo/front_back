<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Endereco extends Model
{
    public $table = 'Endereco';
    public $timestamps = false;
    public $primaryKey = 'ID_ENDERECO';

    public function Cliente()
    {
        return $this->hasOne(Cliente::class, 'ID_CLIENTE', 'ID_CLIENTE');
    }
}
