<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
  public $table = 'Cliente';
  public $timestamps = false;
  public $primaryKey = 'ID_CLIENTE';

   public function Endereco(){
      return $this->hasMany(Endereco::class, "ID_CLIENTE", "ID_CLIENTE");
   }
}
