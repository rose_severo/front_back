<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venda extends Model
{
    public $table = 'Venda';
    public $primaryKey = 'ID_VENDA';
    public $timestamps = false;

    public function Cliente(){
       return $this->hasOne(Cliente::class, "ID_CLIENTE", "ID_CLIENTE");
    }

    public function Produto(){
       return $this->hasOne(Produto::class, "ID_PRODUTO", "ID_PRODUTO");
    }
}
