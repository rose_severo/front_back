<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    public $table = 'Produto';
    public $primaryKey = 'ID_PRODUTO';
    public $timestamps = false;

    public function maisVendidos(){
      $idsProdutos = $this->newQuery()
        ->select(\DB::raw('count(ID_PRODUTO) as quantidade_vendas, ID_PRODUTO'))
        ->from('VendaProduto')
        ->groupBy('ID_PRODUTO')
        ->orderBy('quantidade_vendas', 'desc')
        ->get();

      if (!$idsProdutos) {
          return collect();
      }

        $idsProdutosWhere = [];
        foreach ($idsProdutos as $idProduto) {
          $idsProdutosWhere[] = $idProduto->ID_PRODUTO;
        }

      return $this->newQuery()
        ->whereIn('id_produto', $idsProdutosWhere)
        ->get();
    }
}
