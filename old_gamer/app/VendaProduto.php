<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VendaProduto extends Model
{
    public $table = 'VendaProduto';
    public $primaryKey = 'ID_VENDAPRODUTO';
    public $timestamps = false;

    public function Venda(){
       return $this->hasOne(Venda::class, "ID_VENDA", "ID_VENDA");
    }

    public function Produto(){
       return $this->hasOne(Produto::class, "ID_PRODUTO", "ID_PRODUTO");
    }
}
