<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produto;

class ProdutoController extends Controller
{
    public function index($tipoProduto = null)
    {
        if (!$tipoProduto) {
            $produtos = Produto::all();
        } else {
            $produtos = Produto::query()
                ->where('TIPO_PRODUTO', $tipoProduto)
                ->get();
        }
        
        return view('produto/index', [
            'produtos' => $produtos
        ]);
    }

    public function apiGetProduct($idProduto)
    {
        $produto = Produto::find($idProduto);

        if (!$produto) {
            return response()
                ->json([
                    'error' => true,
                    'message' => 'Produto não encontrado.'
                ]);
        }

        $produto->IMAGEM = base64_encode($produto->IMAGEM);

        return response()
            ->json($produto->attributesToArray());
    }
}
