<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produto;

class InicioController extends Controller
{
    public function index()
    {
        $produtos = Produto::all();
        $produtosMaisVendidos = (new Produto)->maisVendidos();

        return view('index', [
          'produtos' => $produtos,
          'maisVendidos' => $produtosMaisVendidos
        ]);
    }
}
