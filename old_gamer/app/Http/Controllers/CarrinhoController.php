<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produto;
use App\Venda;
use App\VendaProduto;

class CarrinhoController extends Controller
{
    public function index(Request $request)
    {
        $produtosCarrinho = $request->session()->get("carrinho.produtos");

        return view('carrinho/index', [
            "produtos" => $produtosCarrinho
        ]);
    }

    public function finalizar(Request $request)
    {
        $produtos = collect($request->session()->get('carrinho.produtos'));

        $vendaModel = new Venda();
        $vendaModel->ID_CLIENTE = $request->session()->get('cliente')->ID_CLIENTE;
        $vendaModel->ID_PRODUTO = 0;
        $vendaModel->VALOR_TOTAL = $produtos->sum('VALOR');

        \DB::beginTransaction();

        try {
            $vendaModel->save();

            $produtos->map(function($produto) use ($vendaModel) {
                $vendaProduto = new VendaProduto();
                $vendaProduto->ID_VENDA = $vendaModel->ID_VENDA;
                $vendaProduto->ID_PRODUTO = $produto->ID_PRODUTO;
                $vendaProduto->save();
            });

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            return redirect()
                ->back()
                ->with('message', 'Não foi possível efetuar o pedido.');
        }

        $request->session()->put('carrinho.produtos', []);

        return redirect()
            ->back()
            ->with('message', 'Pedido realizado com sucesso!');
    }

    public function removeCarrinho(Request $request, $indice)
    {
        $produtos = $request->session()->get('carrinho.produtos');

        unset($produtos[$indice]);

        $request->session()->put('carrinho.produtos', $produtos);

        return redirect()
            ->route('carrinho.index');
    }

    public function addCarrinho(Request $request, $produtoId)
    {
        $produto = Produto::find($produtoId);

        if (!$produto) {
            return response()
                ->json([
                    'error' => true,
                    'message' => 'Produto não encontrado.'
                ]);
        }

        if (!$request->session()->get('cliente')) {
            return response()
                ->json([
                    'error' => true,
                    'message' => 'Você deve estar logado para adicionar um produto no carrinho de compras.'
                ]);
        }

        $request->session()->push('carrinho.produtos', $produto);

        return response()
            ->json([
                'error' => false,
                'message' => 'Produto adicionado no carrinho de compras.'
            ]);
    }
}
