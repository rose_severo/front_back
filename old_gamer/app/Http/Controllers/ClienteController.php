<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente; 
use App\Endereco;

class ClienteController extends Controller
{
    public function cadastro(Request $request)
    {
        if (!$request->all()) {
            return view('cliente/cadastro');
        }

        $cliente = new Cliente;
        $cliente->NOME_COMPLETO = $request->get('nome');
        $cliente->CPF = $request->get('cpf');
        $cliente->RG = $request->get('rg');
        $cliente->EMAIL = $request->get('email');
        $cliente->SENHA = $request->get('senha');
        $cliente->TELEFONE1 = $request->get('telefone1');
        $cliente->TELEFONE2 = $request->get('telefone2');
        $cliente->DATA_NASCIMENTO = $request->get('data-nascimento');

        $endereco = new Endereco;
        $endereco->ESTADO = $request->get('estado');
        $endereco->CIDADE = $request->get('cidade');
        $endereco->BAIRRO = $request->get('bairro');
        $endereco->NUMERO = $request->get('numero');
        $endereco->LOGRADOURO = $request->get('endereco');
        $endereco->COMPLEMENTO = !empty($request->get('complemento')) ? $request->get('complemento') : '';

        try {
            $cliente->save();
            $endereco->ID_CLIENTE = $cliente->ID_CLIENTE;
            $endereco->save();
        } catch (\Exception $e) {
            return redirect()
                ->back()
                ->with('message', 'Não foi possível completar o cadastro.');
        }

        return redirect()
            ->route('login')
            ->with('message', 'Cadastro efetuado com sucesso! Efetue login por favor.');
    }

    public function editar(Request $request)
    {
        if (!$request->all()) {
            return view('cliente/editar');
        }

        $id = $request->session()->get('cliente')->ID_CLIENTE;

        $cliente = Cliente::find($id);

        $cliente->NOME_COMPLETO = $request->get('nome');
        $cliente->TELEFONE1 = $request->get('telefone1');
        $cliente->TELEFONE2 = $request->get('telefone2');
        $cliente->DATA_NASCIMENTO = $request->get('data-nascimento');

        if (!empty($request->get('senha'))) {
            $cliente->SENHA = $request->get('senha');
        }

        $endereco = $cliente->Endereco->first();

        $endereco->LOGRADOURO = $request->get('endereco');
        $endereco->NUMERO = $request->get('numero');
        $endereco->ESTADO = $request->get('estado');
        $endereco->CIDADE = $request->get('cidade');
        $endereco->BAIRRO = $request->get('bairro');

        if (!empty($request->get('complemento'))) {
            $endereco->COMPLEMENTO = $request->get('complemento');
        }

        try {
            $cliente->save();
            $endereco->save();
        } catch (\Exception $e) {
            return redirect()
                ->back()
                ->with('message', 'Não foi possível atualizar o cadastro.');
        }

        $request->session()->put('cliente', $cliente);

        return redirect()
            ->back()
            ->with('message', 'Cliente atualizado com sucesso!');
    }
}
