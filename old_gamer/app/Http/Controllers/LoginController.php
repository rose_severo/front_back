<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produto;
use App\Cliente;
use App\Admin;

class LoginController extends Controller
{
    public function index(Request $request)
    {
        if ($request->session()->has('logado')) {
            return redirect()
                ->route('index');
        }

        if ($request->all()) {
            $admin = Admin::query()
                ->where('EMAIL', $request->get('email'))
                ->where('SENHA', $request->get('senha'))
                ->first();

            if ($admin) {
                $request->session()->put('logado', true);
                $request->session()->put('admin', $admin);   

                return redirect()
                    ->route('index');
            }

            $cliente = Cliente::query()
                ->where('EMAIL', $request->get('email'))
                ->where('SENHA', $request->get('senha'))
                ->first();

            if (!$cliente) {
                return redirect()
                    ->back()
                    ->with('message', 'Usuário ou senha inválido.');
            }

            $request->session()->put('logado', true);
            $request->session()->put('cliente', $cliente);

            return redirect()
                ->route('index');
        }

        return view('login/index');
    }

    public function logout(Request $request)
    {
        $request->session()->remove('logado');
        $request->session()->remove('cliente');
        $request->session()->remove('admin');

        return redirect()
            ->route('login');
    }
}
