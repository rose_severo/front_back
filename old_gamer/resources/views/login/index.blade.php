@extends('master')
@section('page_title', 'Old Gamer - Login')
@section('content')
    <main>
        <div class="row" id="login">
            <div class="col-md-12 text-center">
                <img src="{{ asset('_img/logos/mario-yoshi.png') }}" alt="Contato" id="logo-pag-games" class="m-auto" />
                <div class="row">
                    <div class="col-md-3 m-auto">
                        <form method="post">
                             {!! csrf_field() !!}

                            <div class="input-group">
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                </div>
                                <input type="text" placeholder="E-mail" class="form-control" name="email">
                            </div>
                            <br/>
                            <div class="input-group">
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="fa fa-lock"></i></span>
                                </div>
                                <input type="password" placeholder="Senha" class="form-control" name="senha">
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="#" id="esqueci-senha">Esqueci minha senha</a>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="{{ route('cliente.cadastro') }}" class="font-weight-bold">Ainda não é cadastrado?</a>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="submit" class="btn btn-warning" name="login-entrar" value="Entrar">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
