<div class="row p-5">
    <div class="col-md-4 text-center" id="redes-sociais">
        <h5 class="font-weight-bold">Redes Sociais</h5>
        <div class="row">
            <div class="col-md-12">
                <a href="https://pt-br.facebook.com/" target="_blank">
                    <i class="fab fa-facebook-f" id="face"></i>
                </a>
                <a href="https://www.instagram.com/" target="_blank">
                    <i class="fab fa-instagram" id="insta"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <h5 class="font-weight-bold">Receba Nossas Novidades</h5>
        <div class="row">
            <div class="col-md-6">
                <input type="text" class="form-control" placeholder="Digite seu e-mail">
            </div>
            <div class="col-md-2 p-0">
                <button class="btn btn-secondary">OK</button>
            </div>
        </div>
    </div>
    <div class="col-md-4 text-center">
        <h5 class="font-weight-bold">Dúvidas? Entre em contato!</h5>
        <div class="row">
            <div class="col-md-12">
                <span>
                    oldgamer_retro@gmail.com<br/>
                    (51) 3391-3333/(51) 99125-9999
                </span>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-center">
        <p><i>Desenvolvido por: Denise, Maressa e Rose</i></p>
    </div>
</div>