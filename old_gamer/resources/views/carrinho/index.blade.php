@extends('master')
@section('page_title', 'Old Gamer - Carrinho de compras')
@section('content')
@php
    $produtosCollection = collect($produtos);
@endphp
    <main>
        <div class="container" id="carrinho">
            <br/><br/>
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 class="text-green">Valor total</h2>
                    <h2>R$ {{ number_format($produtosCollection->sum("VALOR"), 2, ',', '.') }}</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <a href="{{ route('carrinho.finalizar') }}" class="btn btn-success">Finalizar Compra</a>
                </div>
            </div>
            <br/><br/>
                <div class="row">
                    @forelse ($produtos as $key => $produto)
                        <div class="col-md-3">
                            <div class="item-carrinho">
                                <a href="#" class="games" data-indice-carrinho="{{ $key }}" data-id-produto="{{ $produto->ID_PRODUTO }}">
                                    <img src="data:image/jpeg;base64,{{ base64_encode($produto->IMAGEM) }}" alt="{{ $produto->TITULO }}" class="img-fluid"/>
                                </a>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <span>{{ $produto->TITULO}}</span>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h4>R$ {{ $produto->VALOR }}</h4>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <a href="{{ route('carrinho.remover', ['indice' => $key]) }}" class=" text-danger">Remover</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                        </div>
                    @empty
                    <p>Carrinho Vazio.</p>
                    @endforelse
                </div>
            <br/>
        </div>
    </main>
@endsection