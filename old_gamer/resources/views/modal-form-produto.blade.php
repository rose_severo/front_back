<div class="modal fade modal-form-produto" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content" id="modal-old-gamer">
      <div class="modal-header">
        <h4 class="modal-title text-center m-auto font-italic">Cadastro de produto</h4>
        <button type="button" class="close ml-0" data-dismiss="modal" aria-label="Fechar">
            <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body d-flex align-items-center justify-content-center">
        <div class="row">
            <div class="col-md-12 text-center">
                <form action="" method="post">
                    <div class="form-group">
                        <input type="file" name="imagem" class="form-control-file" />
                    </div>
                    <div class="form-group">
                        <select name="" class="form-control">
                            <option>Console</option>
                            <option>Jogo</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="text" name="nome_produto" class="form-control" placeholder="Nome do produto" />
                    </div>
                    <div class="form-group">
                        <textarea type="textarea" name="descricao" class="form-control" placeholder="Descrição"></textarea>
                    </div>
                    <div class="form-group">
                        <input type="number" name="valor" class="form-control" placeholder="R$ 0,00">
                    </div>
                    <div class="form-group">
                        <button type="submit" name="cadastrar-produto" class="btn btn-warning">Cadastrar Produto</button>
                    </div>
                </form>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>