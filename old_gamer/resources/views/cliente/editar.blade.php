@extends('master')
@section('page_title', 'Old Gamer - Cadastro de Cliente')
@section('content')

@php
$cliente = Session::get('cliente');
$endereco = $cliente->Endereco->first();
@endphp
    <main>
        <form method="post">
           {!! csrf_field() !!}
            <div class="col-md-12 text-center">
                <h4 class="p-3">Edite seu cadastro e resgate sua infância!</h4>
                <div class="row">
                    <div class="col-md-5 m-auto">
                        <form action="" method="post">
                            <div class="row form-group">
                                <div class="col-md-8">
                                    <input type="text" class="form-control" placeholder="Nome" name="nome" value="{{ $cliente->NOME_COMPLETO }}" required/>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" placeholder="CPF*" name="cpf" value="{{ $cliente->CPF }}" disabled/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="RG" name="rg" value="{{ $cliente->RG }}" disabled/>
                                </div>
                                <div class="col-md-6">
                                    <input type="date" class="form-control" placeholder="Data de nascimento" name="data-nascimento" value="{{ $cliente->DATA_NASCIMENTO }}" required/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-12">
                                    <input type="text" class="form-control" placeholder="Email" name="email" value="{{ $cliente->EMAIL }}" disabled/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="Telefone 1" name="telefone1" value="{{ $cliente->TELEFONE1 }}" required />
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="Telefone 2" name="telefone2" value="{{ $cliente->TELEFONE2 }}" required />
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-9">
                                    <input type="text" class="form-control" placeholder="Endereço" name="endereco" value="{{ $endereco->LOGRADOURO }}" required />
                                </div>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" placeholder="Número" name="numero" value="{{ $endereco->NUMERO }}" required />
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="Estado" name="estado" value="{{ $endereco->ESTADO }}" required />
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="Cidade" name="cidade" value="{{ $endereco->CIDADE }}" required />
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="Bairro" name="bairro" value="{{ $endereco->BAIRRO }}" required />
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="Complemento" value="{{ $endereco->COMPLEMENTO }}" name="complemento" />
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-6">
                                    <input type="password" class="form-control" placeholder="Nova senha" name="senha" />
                                </div>
                                <div class="col-md-6">
                                    <input type="password" class="form-control" placeholder="Confirmar senha" name="confirma-senha" />
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-warning" name="editar">Editar Cadastro</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </form>
        <div class="row">
        </div>
    </main>
    <script>
        $(document).ready(() => {

            $("input[name='cpf']").mask('999.999.999-40');
            $("input[name='rg']").mask('9999999999');

            $('form').submit(() => {
                if ($('[name="senha"]').val() && $('[name="senha"]').val() != $('[name="confirma-senha"]').val()) {
                    alert('Confirmação de senha não confere!');
                    return false;
                }
                return true;
            });

        });
    </script>
@endsection
