@extends('master')
@section('page_title', 'Old Gamer - Cadastro de Cliente')
@section('content')
    <main>
        <form method="post">
           {!! csrf_field() !!}
            <div class="col-md-12 text-center">
                <h4 class="p-3">Realize seu cadastro e resgate sua infância!</h4>
                <div class="row">
                    <div class="col-md-5 m-auto">
                        <form action="" method="post">
                            <div class="row form-group">
                                <div class="col-md-8">
                                    <input type="text" class="form-control" placeholder="Nome" name="nome" required/>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" placeholder="CPF*" name="cpf" required/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="RG" name="rg" required/>
                                </div>
                                <div class="col-md-6">
                                    <input type="date" class="form-control" placeholder="Data de nascimento" name="data-nascimento" required/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-12">
                                    <input type="text" class="form-control" placeholder="Email" name="email" required/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="Telefone Celular" name="telefone1" required />
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="Telefone Fixo" name="telefone2" required />
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-3">
                                    <input type="text" class="form-control" placeholder="CEP" name="cep" />
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-9">
                                    <input type="text" class="form-control" placeholder="Endereço" name="endereco" required />
                                </div>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" placeholder="Número" name="numero" required />
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="Estado" name="estado" required />
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="Cidade" name="cidade" required />
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="Bairro" name="bairro" required />
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="Complemento" name="complemento" />
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-6">
                                    <input type="password" class="form-control" placeholder="Cadastrar senha" name="senha" required />
                                </div>
                                <div class="col-md-6">
                                    <input type="password" class="form-control" placeholder="Confirmar senha" name="confirma-senha" required />
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-warning" name="finaliza-cadastro">Finalizar Cadastro</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </form>
        <div class="row">
        </div>
    </main>
    <script>
        $(document).ready(() => {

            $("input[name='cpf']").mask('999.999.999-40');
            $("input[name='rg']").mask('9999999999');
            $("input[name='telefone1']").mask('(99) 99999-9999');
            $("input[name='telefone2']").mask('(99) 9999-9999');

            $('form').submit(() => {
                if ($('[name="senha"]').val() != $('[name="confirma-senha"]').val()) {
                    alert('Confirmação de senha não confere!');
                    return false;
                }
                return true;
            });

            let fieldCep = $('[name="cep"]');
            fieldCep.mask('00000-000');

            fieldCep.keyup(e => {
                let value = $(e.target).val().replace('-', '');

                if (value.length !== 8 || !value) {
                    return;
                }

                const url = 'https://viacep.com.br/ws/' + value + '/json/';

                fetch(url).then(response => {
                    response.json().then(body => {

                        if (body.erro) {
                            return;
                        }

                        let address = $('[name="endereco"]'),
                            neighborhood = $('[name="bairro"]'),
                            state = $('[name="estado"]'),
                            city = $('[name="cidade"]');

                        if (body.logradouro) {
                            address.val(body.logradouro);
                        }

                        if (body.bairro) {
                            neighborhood.val(body.bairro);
                        }

                        state.val(body.uf);
                        city.val(body.localidade);

                        $('[name="numero"]').val('').focus();
                    });
                }).catch(err => {
                    console.log(err);
                });
            });
        });
    </script>
@endsection
