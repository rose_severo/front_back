@extends('master')
@section('page_title', 'Old Gamer - Contato')
@section('content')
    <main>
        <div class="row" id="contato">
            <div class="col-md-12 text-center">
                <h4 class="p-3">Central de atendimento</h4>
                <div class="row">
                    <div class="col-md-12">
                        <i class="fab fa-whatsapp" id="whats"></i>
                        <div class="row">
                            <div class="col-md-12">
                                <span>WHATSAPP</span>
                                <div class="row">
                                    <div class="col-md-12">
                                        <span class="font-italic">(51) 99125-9999</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-12">
                                <span class="font-weight-bold">Horário de atendimento:</span>
                                <div class="row">
                                    <div class="col-md-12">
                                        <span class="text-danger font-weight-bold">Seg à sex - 9h às 18h</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-12">
                        <i class="fa fa-envelope" id="email"></i>
                        <div class="row">
                            <div class="col-md-12">
                                <span>E-MAIL</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <span class="font-italic">oldgamer_retro@gmail.com</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection