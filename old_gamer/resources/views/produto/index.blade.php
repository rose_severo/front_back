@extends('master')
@section('page_title', 'Old Gamer - Produtos')
@section('content')
    <main>
        <div class="row">
            <div class="col-md-12 d-flex flex-column">
                <img src="{{ asset('_img/logos/cog-mario.png') }}" alt="Games Diversos" id="logo-pag-games" class="m-auto" />
            </div>
        </div>
        <br/>
        @if (Session::has('admin'))
            <div class="row">
                <div class="col-md-12 text-center mb-4">
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target=".modal-form-produto">Adicionar +</button>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="row p-4">
                @foreach ($produtos as $produto)
                    <div class="col-md-3 text-center games mb-4" data-id-produto="{{ $produto->ID_PRODUTO }}">
                        <div class="games-item">
                            <a href="#" data-toggle="modal" data-target="modal-produto">
                                <img src="data:image/jpeg;base64,{{ base64_encode($produto->IMAGEM) }}" alt="{{ $produto->TITULO }}" class="img-fluid"/>
                            </a>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <span>{{ $produto->TITULO }}</span>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4>R$ {{ $produto->VALOR }}</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                </div>
            </div>
        </div>
    </main>
@endsection