<div class="modal fade modal-produto" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content" id="modal-old-gamer">
      <div class="modal-header">
        <h4 class="modal-title text-center m-auto font-italic" id="titulo-produto"></h4>
        <button type="button" class="close ml-0" data-dismiss="modal" aria-label="Fechar">
            <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body d-flex align-items-center">
        <div class="row">
            <div class="col-md-5 text-center">
                <div class="item-modal">
                    <a href="#">
                      <img src="" alt="Imagem Produto" class="img-fluid" id="imagem-produto"/>
                    </a>
                </div>
            </div>
            <div class="col-md-7 text-center">
                <p id="descricao-produto"></p>
                <p id="valor-produto" class="font-weight-bold"></p>
                <div class="row">
                    <div class="col-md-12">
                        <button type="button" class="btn btn-warning" id="comprar">Comprar</button>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>