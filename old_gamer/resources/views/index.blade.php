@extends('master')

@section('content')
    <header>
        <div class="row m-0">
            <div class="col-md-12 background-image p-0"></div>
        </div>
    </header>

    <main>
        <div class="row align-items-center" id="icones-center">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-4 text-center">
                        <i class="fa fa-credit-card"></i>
                        <br/>
                        <div class="row pt-3">
                            <div class="col-md-12">
                                    <span>
                                        FAÇA SUA COMPRA<br/>
                                        COM SEGURANÇA!<br/>
                                        Site 100% seguro
                                    </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 text-center">
                        <i class="fa fa-lock"></i>
                        <br/>
                        <div class="row pt-3">
                            <div class="col-md-12">
                                    <span>
                                        PARCELE EM ATÉ<br/>
                                        10X SEM JUROS
                                    </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 text-center">
                        <i class="fa fa-comments"></i>
                        <br/>
                        <div class="row pt-3">
                            <div class="col-md-12">
                                    <span>
                                        MAIS DE 3 MIL CLIENTES<br/>
                                        RESGATE SUA INFÂNCIA
                                    </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="row d-flex justify-content-around">
                    <div class="col-md-3">
                        <div class="d-flex justify-content-center p-3" id="mais-vendidos">
                            <span>Mais Vendidos</span>
                        </div>
                        <br/>
                        @foreach ($maisVendidos as $produto)
                        <div class="row">
                            <div class="col-md-12 text-center games" data-id-produto="{{ $produto->ID_PRODUTO }}">
                                <div class="games-item">
                                    <a href="#">
                                        <img src="data:image/jpeg;base64,{{ base64_encode($produto->IMAGEM) }}" alt="Jogo Super Mário" class="img-fluid">
                                    </a>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <span><strong>{{ $produto->TITULO }}</strong></span>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <span>R$ {{ $produto->VALOR }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br/>
                        @endforeach
                    </div>
                    <div class="col-md-7">
                        <div class="d-flex align-items-center p-3" id="lancamentos">
                            <span>Lançamentos</span>
                        </div>
                        <br/>
                        <div class="row">
                        @foreach ($produtos as $produto)
                            <div class="col-md-4 text-center games mb-3" data-id-produto="{{ $produto->ID_PRODUTO }}">
                                <div class="games-item">
                                    <a href="#">
                                        <img src="data:image/jpeg;base64,{{ base64_encode($produto->IMAGEM) }}" alt="Jogo Super Mário" class="img-fluid">
                                    </a>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <span><strong>{{ $produto->TITULO }}</strong></span>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <span>R$ {{ $produto->VALOR }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        </div>
                        <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
