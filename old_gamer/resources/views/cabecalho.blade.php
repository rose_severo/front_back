<div class="row w-100 m-0 align-items-center" id="menu-topo">
    <div class="col-md-2 pl-4 pb-1 d-flex align-items-center justify-content-between">
        <a href="{{ route('index') }}">
            <img src="{{ asset('_img/logos/logo.png') }}" class="img-fluid" id="logo-principal" alt="Logo">
        </a>
        <div class="d-xl-none" id="hamburguer">
            <i class="fas fa-bars"></i>
        </div>
    </div>
    <div class="col-md-10" id="nav-mobile">
        <div class="row">
            <div class="col-md-4 d-none d-xl-block">
                <div class="input-group-append d-flex align-items-center justify-content-end border-bottom">
                    <input type="text" class="form-control bg-transparent position-relative border-0" placeholder="Busca" name="busca-geral"/>
                    <span class="input-group-text bg-transparent border-0 position-absolute"><i class="fa fa-search"></i></span>
                </div>
            </div>
            <div class="col-md-8 p-0">
                <nav class="d-xl-flex d-none justify-content-center">
                    <ul class="nav d-block d-xl-flex text-center">
                        <li class="nav-item">
                            <a href="#" class="nav-link">Nossa empresa</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('produto.index', ['tipoProduto' => 'jogo']) }}" class="nav-link">Games Diversos</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('produto.index', ['tipoProduto' => 'console']) }}" class="nav-link">Consoles Diversos</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('contato') }}" class="nav-link">Contato</a>
                        </li>
                        <li class="nav-item">
                            @if (!Session::has('logado'))
                                <a href="{{ route('login') }}" class="nav-link icones-menu">
                                    <i class="fa fa-user-circle"></i>
                                </a>
                            @else
                                @php
                                $nome = '';
                                if (Session::get('cliente')) {
                                    $nome = Session::get('cliente')->NOME_COMPLETO;
                                    $nome = explode(' ', $nome);
                                    $nome = $nome[0];
                                } else {
                                    $nome = Session::get('admin')->NOME;
                                }
                                @endphp

                                <div class="dropdown" id="dropdown-topo">
                                    <a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link">{{ $nome }}</a>
                                    <ul class="dropdown-menu mt-4">
                                        @if (Session::has('cliente'))
                                        <a href="{{ route('cliente.editar') }}" class="dropdown-item text-dark nav-link">Editar Perfil</a>  
                                        <div class="dropdown-divider"></div>
                                        @endif
                                        <a href="{{ route('logout') }}" class="dropdown-item text-dark nav-link">Logout</a>
                                    </ul>
                                </div>
                            @endif
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('carrinho.index') }}" class="nav-link icones-menu">
                                <i class="fa fa-shopping-cart"></i>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>