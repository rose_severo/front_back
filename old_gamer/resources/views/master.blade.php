@php
if (empty($message)) {
    $message = session('message');
}
@endphp
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{ asset('_js/jquery-3.4.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('_js/jquery.mask.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bootstrap4/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('_js/javascript-geral.js') }}"></script>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('bootstrap4/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('_css/style.css') }}">

    <title>@yield('page_title', 'Old Gamer - Página Inicial')</title>
</head>
<body>
<div class="container-fluid p-0">
    @include('cabecalho')

    @include('modal-produto')
    @include('modal-form-produto')

    @yield('content')
    <br/>
    <footer class="d-flex flex-column justify-content-center">
        @include('footer')
    </footer>
</div>
</body>
<script>
$(document).ready(() => {
  @if ($message)
    alert('{{ $message }}');
  @endif

    $('.games').click(e => {
        let idProduto = e.currentTarget.getAttribute('data-id-produto'),
            url = "{{ route('api.produto.get', ['id' => '']) }}/" + idProduto;

        fetch(url).then(response => {
            response.json().then(body => {
                $('#titulo-produto').html(body.TITULO);
                $('#descricao-produto').html(body.DESCRICAO);
                $('#imagem-produto').attr('src', 'data:image/jpeg;base64,' + body.IMAGEM);
                $("#valor-produto").html("R$ " + body.VALOR);
                $("button#comprar").attr("data-id-produto", body.ID_PRODUTO);
                $('.modal-produto').modal('show');
            });
        });

        return false;
    });

    $('button#comprar').click(e => {
        let idProduto = e.currentTarget.getAttribute('data-id-produto'),
            url = "{{ route('api.carrinho.add', ['produtoId' => '']) }}/" + idProduto;
        
        fetch(url).then(response => {
            response.json().then(body => {
                if (body.error) {
                    alert(body.message);
                    return false;
                }
                window.location = "{{ route('carrinho.index') }}";
            });
        }).catch(error => {
            console.log(error);
        });
    });
});

</script>
<script type="text/javascript" src="{{ asset('_js/product.js') }}"></script>
</html>
